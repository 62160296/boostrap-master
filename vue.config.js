module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160296/learn_bootstrap/'
      : '/'
}
